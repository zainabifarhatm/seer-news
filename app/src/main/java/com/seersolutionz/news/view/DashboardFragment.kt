package com.seersolutionz.news.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.seersolutionz.news.R
import com.seersolutionz.news.databinding.FragmentDashboardBinding
import com.seersolutionz.news.databinding.FragmentLoginBinding
import com.seersolutionz.news.viewmodel.NewsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : Fragment() {
    private val viewModel: NewsViewModel by viewModels()
    private lateinit var binding: FragmentDashboardBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return getBindingView(container)
    }

    private fun getBindingView(container: ViewGroup?): View? {
        binding = FragmentDashboardBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getTopStories()
        handleUIStateChanges()
        handleSearch()
    }

    private fun handleSearch() {

        binding.search.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.getSearchResults(v.text.toString())
                val mainActivity = activity as MainActivity
                mainActivity.hideKeyboard()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    private fun handleUIStateChanges() {
        viewModel.uiState.observe(viewLifecycleOwner, Observer {
            when(it) {
                is NewsViewModel.UIState.TopStoriesFetched -> {
                    binding.topStories.adapter = TopStoriesAdapter(it.topStories)
                    binding.topStories.layoutManager = LinearLayoutManager(
                        requireContext(), LinearLayoutManager.HORIZONTAL, false)
                }

                is NewsViewModel.UIState.Error ->
                    Toast.makeText(requireContext(), getString(it.errorMessageId),
                        Toast.LENGTH_LONG).show()

                is NewsViewModel.UIState.SearchResultsFetched -> {
                    binding.searchResults.adapter = SearchResultsAdapter(it.searchResults)
                    binding.searchResults.layoutManager = LinearLayoutManager(
                        requireContext(), LinearLayoutManager.VERTICAL, false
                    )
                }

                is NewsViewModel.UIState.NoResults -> {
                    binding.searchResults.visibility = if (!it.noResults) View.VISIBLE else View.GONE
                    binding.noResults.visibility = if (it.noResults) View.VISIBLE else View.GONE
                }
            }
        })
    }
}