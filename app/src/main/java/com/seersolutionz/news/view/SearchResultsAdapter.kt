package com.seersolutionz.news.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.seersolutionz.news.databinding.ItemSearchResultsBinding
import com.seersolutionz.news.databinding.ItemTopStoriesBinding
import com.seersolutionz.news.repository.model.News
import com.seersolutionz.news.repository.model.TopStory

class SearchResultsAdapter(private val searchResults: List<News>
): RecyclerView.Adapter<SearchResultsAdapter.NewsHolder>() {


    class NewsHolder(binding: ItemSearchResultsBinding) : RecyclerView.ViewHolder(binding.root) {
        private val binding: ItemSearchResultsBinding
        private var title: MutableLiveData<String> = MutableLiveData()
        private var abstract: MutableLiveData<String> = MutableLiveData()

        init {
            this.binding = binding
            binding.title = title
            binding.description = abstract
        }

        fun bind(title: String, abstract: String) {
            this.title.value = title
            this.abstract.value = abstract
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemSearchResultsBinding.inflate(layoutInflater, parent, false)
        return NewsHolder(binding)
    }

    override fun onBindViewHolder(holder: NewsHolder, position: Int) {
        holder.bind(searchResults.get(position).abstract, searchResults.get(position).leadParagraph)
    }

    override fun getItemCount(): Int {
        return searchResults.size
    }
}