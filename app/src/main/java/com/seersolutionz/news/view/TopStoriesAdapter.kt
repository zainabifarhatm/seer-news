package com.seersolutionz.news.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.seersolutionz.news.databinding.ItemTopStoriesBinding
import com.seersolutionz.news.repository.model.TopStory

class TopStoriesAdapter(private val topStories: List<TopStory>
): RecyclerView.Adapter<TopStoriesAdapter.NewsHolder>() {


    class NewsHolder(binding: ItemTopStoriesBinding) : RecyclerView.ViewHolder(binding.root) {
        private val binding: ItemTopStoriesBinding
        private var title: MutableLiveData<String> = MutableLiveData()
        private var abstract: MutableLiveData<String> = MutableLiveData()

        init {
            this.binding = binding
            binding.title = title
            binding.description = abstract
        }

        fun bind(title: String, abstract: String) {
            this.title.value = title
            this.abstract.value = abstract
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemTopStoriesBinding.inflate(layoutInflater, parent, false)
        return NewsHolder(binding)
    }

    override fun onBindViewHolder(holder: NewsHolder, position: Int) {
        holder.bind(topStories.get(position).title, topStories.get(position).abstract)
    }

    override fun getItemCount(): Int {
        return topStories.size
    }
}