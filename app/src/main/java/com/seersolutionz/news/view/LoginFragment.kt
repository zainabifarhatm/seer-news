package com.seersolutionz.news.view

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.seersolutionz.news.R
import com.seersolutionz.news.databinding.FragmentLoginBinding
import com.seersolutionz.news.viewmodel.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private val viewModel: LoginViewModel by viewModels()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return getBindingView(container)
    }

    private fun getBindingView(container: ViewGroup?): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Handle login click
        binding.login.setOnClickListener {
            val mainActivity = activity as MainActivity
            mainActivity.hideKeyboard()
            viewModel.onLogin(
                    binding.username.editText?.text.toString(),
                    binding.password.editText?.text.toString())
        }

        // Handle UI States
        viewModel.uiState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is LoginViewModel.UIState.UsernameError ->
                    binding.username.error = getString(R.string.required)
                is LoginViewModel.UIState.PasswordError ->
                    binding.password.error = getString(R.string.required)
                is LoginViewModel.UIState.SuccessfulLogin -> {
                    findNavController().navigate(R.id.action_loginFragment_to_dashboardFragment)
                }
            }
        })
    }
}