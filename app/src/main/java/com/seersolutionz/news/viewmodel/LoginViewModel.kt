package com.seersolutionz.news.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.seersolutionz.news.repository.LoginRepository
import com.seersolutionz.news.repository.Resource
import com.seersolutionz.news.repository.model.LoginRequest
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
        private val repository: LoginRepository
) : ViewModel() {
    private val _uiState = MediatorLiveData<UIState>()
    val uiState: LiveData<UIState>
        get() = _uiState

    sealed class UIState {
        object UsernameError: UIState()
        object PasswordError: UIState()
        object SuccessfulLogin: UIState()
    }

    /**
     * Validate login fields
     */
    fun onLogin(username: String, password: String) {
        when {
            username.trim().isEmpty() -> _uiState.value = UIState.UsernameError
            password.trim().isEmpty() -> _uiState.value = UIState.PasswordError
            else -> {
                _uiState.addSource(repository.login(LoginRequest(username.trim(), password.trim())),
                        Observer {
                    // Considering only success login
                            if (it.status == Resource.Status.SUCCESS)
                                _uiState.value = UIState.SuccessfulLogin
                })
            }
        }
    }
}