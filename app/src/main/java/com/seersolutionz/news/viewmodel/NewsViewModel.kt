package com.seersolutionz.news.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.seersolutionz.news.repository.NewsRepository
import com.seersolutionz.news.repository.Resource
import com.seersolutionz.news.repository.model.News
import com.seersolutionz.news.repository.model.TopStoriesResponse
import com.seersolutionz.news.repository.model.TopStory
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NewsViewModel @Inject constructor(
    private var repository: NewsRepository
): ViewModel() {
    private val _uiState = MediatorLiveData<UIState>()
    val uiState: LiveData<UIState>
        get() = _uiState

    sealed class UIState {
        object Loading : UIState()
        object LoadingCompleted : UIState()
        class Error(val errorMessageId: Int) : UIState()
        class TopStoriesFetched(val topStories: List<TopStory>) : UIState()
        class SearchResultsFetched(val searchResults: List<News>) : UIState()
        class NoResults(val noResults: Boolean): UIState()
    }

    /**
     * Fetch top stories results from repository
     * Update UI State based on the response
     */
    fun getTopStories() {
        _uiState.addSource(repository.getTopStories(), Observer {
            when (it.status) {
                Resource.Status.LOADING -> _uiState.value = UIState.Loading
                Resource.Status.ERROR -> {
                    _uiState.value = UIState.LoadingCompleted
                    _uiState.value = UIState.Error(it.errorMessageId!!)
                }
                Resource.Status.SUCCESS -> {
                    _uiState.value = UIState.LoadingCompleted
                    _uiState.value = UIState.TopStoriesFetched(it.data?.results!!)
                }
            }
        })
    }

    /**
     * Fetch top stories results from repository
     * Update UI State based on the response
     */
    fun getSearchResults(keyword: String) {
        _uiState.addSource(repository.getSearchResults(keyword), Observer {
            when (it.status) {
                Resource.Status.LOADING -> _uiState.value = UIState.Loading
                Resource.Status.ERROR -> {
                    _uiState.value = UIState.LoadingCompleted
                    _uiState.value = UIState.Error(it.errorMessageId!!)
                }
                Resource.Status.SUCCESS -> {
                    _uiState.value = UIState.LoadingCompleted
                    if (it.data?.response?.docs!!.isEmpty()) {
                        _uiState.value = UIState.NoResults(true)
                    } else {
                        _uiState.value = UIState.NoResults(false)
                        _uiState.value = UIState.SearchResultsFetched(it.data.response.docs)
                    }
                }
            }
        })
    }
}