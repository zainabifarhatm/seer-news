package com.seersolutionz.news.utils

import com.seersolutionz.news.BuildConfig
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull

/**
 * Interceptor to mock Login Request
 */
class MockInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (BuildConfig.DEBUG) {
            val uri = chain.request().url.toUri().toString()
            return if (uri.endsWith("login")) {
                chain.proceed(chain.request())
                        .newBuilder()
                        .code(200)
                        .protocol(Protocol.HTTP_2)
                        .message(mockLoginResponse)
                        .body(ResponseBody.create("application/json".toMediaTypeOrNull(),
                                mockLoginResponse.toByteArray()))
                        .addHeader("content-type", "application/json")
                        .build()
            } else
                chain.proceed(chain.request())
        } else {
            //just to be on safe side.
            throw IllegalAccessError("MockInterceptor is only meant for Testing Purposes and " +
                    "bound to be used only with DEBUG mode")
        }
    }
}

const val mockLoginResponse = """
{
    "status": "OK",
	"name": "Hello-World",
    "token": "hdufgdsjfdsjfbsdjfbsdjbfjsdbfsdjfbsdjbfjsdbf"
   
}
"""