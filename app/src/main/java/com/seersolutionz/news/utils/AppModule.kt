package com.seersolutionz.news.utils

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.seersolutionz.news.repository.service.LoginService
import com.seersolutionz.news.repository.service.NewsService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Singleton
    @Named("MockInterceptor")
    fun provideMockInterceptor(): Interceptor = MockInterceptor()

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit {
        val client: OkHttpClient = OkHttpClient.Builder()
                .addInterceptor(MockInterceptor())
                .addInterceptor(HttpLoggingInterceptor()
                        .apply { level = HttpLoggingInterceptor.Level.BODY })
                .build()

        return Retrofit.Builder()
            .baseUrl("https://api.nytimes.com/svc/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideNewsService(retrofit: Retrofit): NewsService = retrofit.create(
        NewsService::class.java
    )

    @Provides
    fun provideLoginService(retrofit: Retrofit): LoginService = retrofit.create(
            LoginService::class.java
    )
}