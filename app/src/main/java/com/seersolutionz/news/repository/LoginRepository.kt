package com.seersolutionz.news.repository

import com.seersolutionz.news.repository.model.LoginRequest
import com.seersolutionz.news.repository.source.LoginAPISource
import com.seersolutionz.news.repository.source.NewsAPISource
import javax.inject.Inject

class LoginRepository @Inject constructor(
    private val apiSource: LoginAPISource
){
    fun login(request: LoginRequest) = performGetOperation { apiSource.login(request) }

}