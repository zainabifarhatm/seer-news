package com.seersolutionz.news.repository.source

import com.seersolutionz.news.R
import com.seersolutionz.news.repository.Resource
import com.seersolutionz.news.repository.model.BaseAPIResponse
import retrofit2.Response

const val API_FAILED = 1000

abstract class BaseAPISource {

    // Add all possible error codes here
    private val errorCodes = mapOf(
        1000 to R.string.unknown_error
    )

    /**
     * When API response is 200 OK, check for success status in response body.
     * If status in response body is SUCCESS, call the success method
     * If status in response body is ERROR, call the error method with error message string res id
     * For all other errors, show common error
     */

    protected suspend fun<T> getAPIResult(
        call: suspend () -> Response<T>): Resource<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val responseBody = response.body() as BaseAPIResponse
                if (responseBody.status == "OK") {
                    val responseData = responseBody as T
                    return Resource.success(responseData)
                }
                // Handling a common error for now
                return Resource.error(errorCodes.get(API_FAILED)!!)
            }
            return Resource.error(errorCodes.get(API_FAILED)!!)
        } catch (e: Exception) {
            return Resource.error(errorCodes.get(API_FAILED)!!)
        }
    }
}