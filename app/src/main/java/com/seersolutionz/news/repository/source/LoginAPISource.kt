package com.seersolutionz.news.repository.source

import com.seersolutionz.news.repository.Resource
import com.seersolutionz.news.repository.model.BaseAPIResponse
import com.seersolutionz.news.repository.model.LoginRequest
import com.seersolutionz.news.repository.service.LoginService
import javax.inject.Inject

class LoginAPISource @Inject constructor(
    private val requestService: LoginService
): BaseAPISource() {

    suspend fun login(request: LoginRequest): Resource<BaseAPIResponse> = getAPIResult {
        requestService.login(request)
    }
}