package com.seersolutionz.news.repository

data class Resource<T>(val status: Status, val data: T?, val errorMessageId: Int?) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {
        fun <T> success(data: T): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(errorMessageId: Int): Resource<T> {
            return Resource(Status.ERROR, null, errorMessageId)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(Status.LOADING, null, null)
        }
    }
}