package com.seersolutionz.news.repository.service

import com.seersolutionz.news.repository.model.BaseAPIResponse
import com.seersolutionz.news.repository.model.LoginRequest
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

private const val API_KEY = "HqJvV4GLdusnERsA0ShEreYD9lUNAXXN"

interface LoginService {
    @POST("login")
    suspend fun login(@Body request: LoginRequest): Response<BaseAPIResponse>
}