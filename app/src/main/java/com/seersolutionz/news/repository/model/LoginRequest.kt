package com.seersolutionz.news.repository.model

data class LoginRequest(
        var username: String,
        var password: String
)
