package com.seersolutionz.news.repository

import com.seersolutionz.news.repository.source.NewsAPISource
import javax.inject.Inject

class NewsRepository @Inject constructor(
    private val apiSource: NewsAPISource
){
    fun getTopStories() = performGetOperation { apiSource.getTopStories() }
    fun getSearchResults(keyword: String) = performGetOperation { apiSource.getSearchResults(keyword) }

}