package com.seersolutionz.news.repository.source

import com.seersolutionz.news.repository.Resource
import com.seersolutionz.news.repository.model.SearchResultsResponse
import com.seersolutionz.news.repository.model.TopStoriesResponse
import com.seersolutionz.news.repository.service.NewsService
import javax.inject.Inject

class NewsAPISource @Inject constructor(
    private val requestService: NewsService
): BaseAPISource() {

    suspend fun getTopStories(): Resource<TopStoriesResponse> = getAPIResult {
        requestService.getTopStories()
    }

    suspend fun getSearchResults(keyword: String): Resource<SearchResultsResponse> = getAPIResult {
        requestService.getSearchResults(keyword)
    }

}