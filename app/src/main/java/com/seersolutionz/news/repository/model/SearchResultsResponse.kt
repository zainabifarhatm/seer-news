package com.seersolutionz.news.repository.model

import com.google.gson.annotations.SerializedName

data class SearchResultsResponse(
    var response: SearchResult
): BaseAPIResponse()

data class SearchResult (
    var docs: List<News>
)

data class News(
    var abstract: String,
    @SerializedName("lead_paragraph")
    var leadParagraph: String
)
