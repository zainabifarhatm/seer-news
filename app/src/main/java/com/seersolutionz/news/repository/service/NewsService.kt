package com.seersolutionz.news.repository.service

import com.seersolutionz.news.repository.model.SearchResultsResponse
import com.seersolutionz.news.repository.model.TopStoriesResponse
import retrofit2.Response
import retrofit2.http.*

private const val API_KEY = "HqJvV4GLdusnERsA0ShEreYD9lUNAXXN"
private const val CONTENT_TYPE = "application/json"
interface NewsService {
    @Headers("Accept: application/json")
    @GET("topstories/v2/arts.json?api-key=$API_KEY")
    suspend fun getTopStories(): Response<TopStoriesResponse>

    @Headers("Accept: application/json")
    @GET("search/v2/articlesearch.json?api-key=$API_KEY")
    suspend fun getSearchResults(
        @Query("q") keyword: String): Response<SearchResultsResponse>
}