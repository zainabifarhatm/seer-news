package com.seersolutionz.news.repository.model

data class TopStoriesResponse(
    var results: List<TopStory>
): BaseAPIResponse()

data class TopStory(
    var title: String,
    var abstract: String
)
